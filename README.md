# React text editor
This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

## Setup
Run `npm install` in order to install all the required packages

## Development server
Run `npm start` for a dev server.
Run `npm run build` for a build.

## Notes
I wasn't sure if the usage of `contentEditable` elements along with the use of `execCommand()` was allowed or not, therefore i tried to tackle this challenge with a non-traditional approach.

I created the editor from scratch and listened to various input events to emulate some of the basic editor functionalities.
The editor is obviously missing the caret, which should be implemented, but it seemed too complex to implement in the given timeframe. This means that inputs will be registered at the end of the text only.

## Usage
Words must be selected by double clicking the word (highlighting a word manually won't work)

After selecting a word, you can perform any available action over it (for color formatting you will need to have the desired color in the text field beforehand)
To indent a paragraph, you will need to first select any word inside it and then press `tab`

