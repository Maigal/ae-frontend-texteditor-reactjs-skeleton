import React, {Component} from 'react';
import './App.css';
import getMockText from './text.service';
import Editor from './Components/Editor';
import Modal from './Components/Modal';

class App extends Component {

    state = { showModal: false, text: '' }

    componentDidMount() {
        getMockText().then(data => this.setState({text: data}))
    }

    openModal() {
    this.setState({showModal: true})
    }

    closeModal() {
    this.setState({showModal: false})
    }

    render() {
        return (
            <div className="App">
                <header>
                    <span>Simple Text Editor</span>
                </header>
                <main>
                    <Editor text={this.state.text} onOpenModal={() => this.openModal()} />
                    <Modal show={this.state.showModal}  onCloseModal={() => this.closeModal()} />
                </main>
            </div>
        );
    }
}

export default App;
