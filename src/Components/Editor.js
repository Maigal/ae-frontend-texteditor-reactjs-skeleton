import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getStylesFromObject, isHexaColor } from '../helpers';
import { fetchSynonyms } from '../actions';
import './Editor.css';

class Editor extends Component {

  constructor(props) {
    super(props);
    this.colorValue = React.createRef();
    this.paragraphsRef = [];
    this.state = {
      inputText: '',
      output: [],
      selectedParagraph: null,
      selectedIndex: null,
      newWord: null,
      colorValue: '',
      isSpaceKeyActive: false,
      isEnterKeyActive: false
    }
  }


  loadText() {
    const tmpOutput = [];
    this.state.inputText.split('. ').join('.***').split('***').map((paragraph, pIndex) => { // Assumming that '. ' means new paragraph in the input text. 
      tmpOutput[pIndex] = paragraph.split(' ').map((word, wIndex) => {
        return {
          id: wIndex,
          value: word,
          styles: {
            bold: false,
            italic: false,
            underline: false,
            color: false
          }
        }
      })
    })
    this.setState({output: tmpOutput});
  }


  static getDerivedStateFromProps(props, current_state) {
    if(props.text !== current_state.inputText) {
      return {
        inputText: props.text
      }
    }

    if (current_state.newWord !== props.wordReplacement) {
      let tmpState = [...current_state.output];
      tmpState[current_state.selectedParagraph][current_state.selectedIndex].value = props.wordReplacement;
      return {
        newWord: props.wordReplacement,
        output: tmpState
      }
    }
    return null
  }

  componentDidUpdate(prevProps, prevState){
    if (prevState.inputText === "") {
      this.loadText()
    }
  }


  handleInput(ev) {
    const keyPressed = ev.key;

    if (keyPressed === ' ') {
      this.setState({isSpaceKeyActive: true})
    } 
    else if (keyPressed.length ===  1) {
        let _tmpOutput = [...this.state.output];
        let _lastParagraph = _tmpOutput[_tmpOutput.length -1];

        if (this.state.isEnterKeyActive) {
          _tmpOutput.push([{
            id: 0,
            value: keyPressed,
            styles: {
              bold: false,
              italic: false,
              underline: false,
              color: false
            }
          }])
          this.setState({output: _tmpOutput, isEnterKeyActive: false})

        } else if (this.state.isSpaceKeyActive) {
          _lastParagraph.push({
            id: _lastParagraph.length,
            value: keyPressed,
            styles: {
              bold: false,
              italic: false,
              underline: false,
              color: false
          }})
          this.setState({output: _tmpOutput, isSpaceKeyActive: false})

        } else {
          _lastParagraph[_lastParagraph.length - 1].value += keyPressed;
          this.setState({output: _tmpOutput})
        }

    } 

    else if (keyPressed === 'Tab' && this.state.selectedParagraph) {
      let _ref = this.paragraphsRef[this.state.selectedParagraph]
      _ref.style.marginLeft = parseInt(_ref.style.marginLeft, 10) + 30 + 'px'
    }
    
    else if (keyPressed === 'Enter') {
      this.setState({isEnterKeyActive: true});
    }

    else if (keyPressed === 'Backspace') {
      this.setState({isSpaceKeyActive: false});
    }
  }

  selectWord(ev) {
    const _id = ev.target.dataset.id;
    const _paragraphid = ev.target.dataset.paragraphid;
    this.setState({selectedParagraph: _paragraphid, selectedIndex: _id})
  }

  executeAction(ac) {
    if (window.getSelection().type === "Range") {
      let tmpState = [...this.state.output];
      const _paragraphid = this.state.selectedParagraph;
      const _id = this.state.selectedIndex;

      switch (ac) {
        case 'getSynonyms':
          const selectedItem = this.state.output[_paragraphid][_id];
          this.props.fetchSynonyms(selectedItem.value.replace(/\W/g, ''), _paragraphid, _id);
          this.props.onOpenModal();
          break;

        case 'color':
          let colorVal = this.colorValue.current.value;
          if (isHexaColor(colorVal)) {
            tmpState[_paragraphid][_id].styles.color = colorVal;
            this.setState({ouput: tmpState})
          } else {
            alert(`${colorVal} is not a valid HEX color`);
          } 
          break;

        default:
          tmpState[_paragraphid][_id].styles[ac] = !tmpState[_paragraphid][_id].styles[ac];
          this.setState({ouput: tmpState})
          break;
      }

    } else {
      this.setState({selectedParagraph: null, selectedIndex: null});
    }
  }


  renderOutput() {
    return (
      this.state.output.map((paragraph, pIndex) => (
        <p style={{marginLeft: '0'}} key={pIndex} data-index={pIndex} ref={ref => (this.paragraphsRef[pIndex] = ref)}>
          {
            paragraph
            .map(word => (
              <span data-paragraphid={pIndex} data-id={word.id} key={word.id} style={getStylesFromObject(word.styles)} onDoubleClick={(ev) => this.selectWord(ev)}>{word.value}</span>
            ))
            .reduce((prev, curr) => [prev, ' ', curr])
          }
        </p>
      ))
    )
  }


  render() {
    return (
      <div className="editor">
        <div className="editor-menu">
          <button style={{fontWeight: '600'}} onClick={() => this.executeAction('bold')}>B</button>
          <button style={{fontStyle: 'italic'}} onClick={() => this.executeAction('italic')}>I</button>
          <button style={{textDecoration: 'underline'}} onClick={() => this.executeAction('underline')}>U</button>
          <button onClick={() => this.executeAction('color')}>Apply color</button>
          <input ref={this.colorValue} type="text" maxLength="7" size="7" defaultValue="#000000" />
          <button onClick={() => this.executeAction('getSynonyms')}>Replace</button>
        </div>
        
        <div className="editable-area" onKeyDown={(e) => this.handleInput(e)} tabIndex="0">{this.renderOutput()}</div>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    wordReplacement: state.wordReplacement,
    pIndex: state.pIndex,
    wIndex: state.wIndex
  }
}

export default connect(mapStateToProps, {fetchSynonyms})(Editor);
