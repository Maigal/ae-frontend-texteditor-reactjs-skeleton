import React from 'react'
import { connect } from 'react-redux';
import { replaceWord } from '../actions';
import './Modal.css';


const Modal = (props) => {

  const selectWord = (word) => {
    props.replaceWord(word);
    props.onCloseModal()
  }

  const renderModal = () => {
    switch(props.fetchStatus) {
      case 'started':
        return (
          <p>Loading synonyms for {props.queryWord}</p>
        )

      case 'success':
        if (props.synonyms.length > 0) {
          return (
            <React.Fragment>
              <p>Popular synonyms for <strong>{props.queryWord}</strong></p>
              <ul>
                {
                  props.synonyms.slice(0, 5).map((syn, index) => (
                    <li key={index} onClick={() => selectWord(syn.word)}>{syn.word}</li>
                  ))
                }
              </ul>
            </React.Fragment>
          )
        }

        return (
          <p>No synonyms found for <strong>{props.queryWord}</strong></p>
        )
        
      case 'failed':
        return (
          <p>Failed to get synonyms <em>(possibly due to CORS errors)</em></p>
        )
      
      default:
        return null;
    }

  }

  if (props.show) {
    return (
      <div className="dimmer">
        <div className="modal">
          <span className="modal-x" onClick={() => props.onCloseModal()}>X</span>
          {renderModal()}
        </div>
      </div>
    )
  }

  return null
}

const mapStateToProps = (state) => {
  return {
    synonyms: state.synonyms,
    queryWord: state.queryWord,
    fetchStatus: state.fetchStatus
  }
}

export default connect(mapStateToProps, {replaceWord}) (Modal);
