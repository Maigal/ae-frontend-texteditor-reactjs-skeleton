export const fetchSynonyms = word => dispatch => {
  dispatch(fetchSynonymsStarted(word));
  fetch(`https://api.datamuse.com/words?rel_syn=${word}`, {
    method: 'GET'
  })
  .then(response => response.json())
  .then(jsondata => {
    dispatch(fetchSynonymsSuccess(jsondata));
  })
    
  .catch(err => {
    dispatch(fetchSynonymsFailure(err.message));
  });
};

const fetchSynonymsSuccess = synonyms => ({
type: 'FETCH_SYNONYMS_SUCCESS',
payload: synonyms
});

const fetchSynonymsStarted = word => ({
type: 'FETCH_SYNONYMS_STARTED',
payload: word
});

const fetchSynonymsFailure = error => ({
type: 'FETCH_SYNONYMS_FAILURE',
payload: {
  error
}
});


export const replaceWord = word => dispatch => {
  dispatch(replaceWordEmitted(word));
};

const replaceWordEmitted = word => ({
type: 'REPLACE_WORD_EMITTED',
payload: word
});
