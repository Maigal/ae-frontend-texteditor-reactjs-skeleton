export const getStylesFromObject = (oStyles) => {
  let _oStyles = {};
  if (oStyles.bold) { _oStyles.fontWeight = '600' }
  if (oStyles.italic) { _oStyles.fontStyle = 'italic' }
  if (oStyles.underline) { _oStyles.textDecoration = 'underline' }
  if (oStyles.color) { _oStyles.color = oStyles.color }

  return _oStyles;
}

export const isHexaColor = (val) => /(^#[0-9A-F]{6}$)|(^#[0-9A-F]{3}$)/i.test(val);