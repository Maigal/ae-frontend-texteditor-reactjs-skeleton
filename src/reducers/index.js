export default function rootReducer(state = {
  synonyms: [], 
  fetchStatus: '',
  wordReplacement: null
}, action) {
  switch (action.type) {
    case 'FETCH_SYNONYMS_STARTED':
      return {...state, fetchStatus: 'started', queryWord: action.payload };
    case 'FETCH_SYNONYMS_SUCCESS':
      return {
        ...state,
        synonyms: action.payload,
        fetchStatus: 'success'
      };
    case 'FETCH_SYNONYMS_FAILURE':
      return {...state, fetchStatus: 'failed'};
    case 'REPLACE_WORD_EMITTED':
      return {...state, wordReplacement: action.payload};
    default:
      return state;
  }
}